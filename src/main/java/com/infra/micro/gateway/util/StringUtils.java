package com.infra.micro.gateway.util;

import org.springframework.util.AntPathMatcher;
import org.springframework.util.CollectionUtils;

import java.util.List;
/**
 * StringUtils扩展
 *
 * @author PD
 */
public class StringUtils extends org.apache.commons.lang3.StringUtils {
    /**
     * 查找指定字符串是否匹配指定字符串列表中的任意一个字符串
     *
     * @param input    字符串
     * @param patterns 规则列表
     * @return 匹配结果
     */
    public static boolean matches(String input, List<String> patterns) {
        if (org.apache.commons.lang3.StringUtils.isBlank(input) || CollectionUtils.isEmpty(patterns)) {
            return false;
        }
        return patterns.stream().anyMatch(pattern -> isMatch(pattern, input));
    }

    /**
     * 判断url是否与规则配置:
     * ? 表示单个字符;
     * * 表示一层路径内的任意字符串，不可跨层级;
     * ** 表示任意层路径;
     *
     * @param pattern 匹配规则
     * @param path    需要匹配的url
     * @return
     */
    public static boolean isMatch(String pattern, String path) {
        AntPathMatcher matcher = new AntPathMatcher();
        return matcher.match(pattern, path);
    }

}