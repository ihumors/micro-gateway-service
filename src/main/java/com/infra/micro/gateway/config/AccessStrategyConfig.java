package com.infra.micro.gateway.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;

import java.util.ArrayList;
import java.util.List;

/**
 * 白名单配置
 *
 * @author PD
 */
@RefreshScope
@ConfigurationProperties(prefix = "gateway.access")
public class AccessStrategyConfig {
    /**
     * 白名单，支持匿名访问，无需登陆鉴权
     */
    private List<String> ignores = new ArrayList<>();
    /**
     * 黑名单IP列表，直接拒绝访问
     */
    private List<String> denies = new ArrayList<>();
    /**
     * 需鉴权，但忽略账号状态（激活等）
     */
    private List<String> stateless = new ArrayList<>();

    public List<String> getIgnores() {
        return ignores;
    }

    public void setIgnores(List<String> ignores) {
        this.ignores = ignores;
    }

    public List<String> getDenies() {
        return denies;
    }

    public void setDenies(List<String> denies) {
        this.denies = denies;
    }

    public List<String> getStateless() {
        return stateless;
    }

    public void setStateless(List<String> stateless) {
        this.stateless = stateless;
    }
}
