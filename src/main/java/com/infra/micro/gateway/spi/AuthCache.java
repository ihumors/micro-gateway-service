package com.infra.micro.gateway.spi;

/**
 * 请实现这接口
 *
 * @author PD
 */
public interface AuthCache {
    /**
     * 获取指定用户名的Token
     *
     * @param userName
     * @return
     */
    String get(String userName);

    /**
     * 重置指定用户名的Token的过期时长
     *
     * @param userName
     * @return
     */
    boolean refreshExpire(String userName);
}
