package com.infra.micro.gateway.spi.impl;

import com.goshine.zhqj.redis.cache.SysAuthCache;
import com.infra.micro.gateway.spi.AuthCache;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * 请根据自己的业务场景重写此实现类
 *
 * @author PD
 */
@Component
@ComponentScan(value = {"com.goshine"})
public class AuthCacheImpl implements AuthCache {
    @Resource
    private SysAuthCache sysAuthCache;

    @Override
    public String get(String userName) {
        return sysAuthCache.get(userName);
    }

    @Override
    public boolean refreshExpire(String userName) {
        return sysAuthCache.refreshExpire(userName);
    }
}
